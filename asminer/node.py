import logging
import json

import requests
#import bs4

from minemeld.ft.basepoller import BasePollerFT

LOG = logging.getLogger(__name__)


class Asreader(BasePollerFT):
    def configure(self):
        super(Asreader, self).configure()

        self.polling_timeout = self.config.get('polling_timeout', 20)
        self.verify_cert = self.config.get('verify_cert', True)

        self.asnumber = self.config.get('as_number', None)
        if self.asnumber is None:
            raise ValueError('%s - AS Number is required' % self.name)
        self.url = 'https://stat.ripe.net/data/announced-prefixes/data.json?preferred_version=1.1&resource={}'.format(
            self.asnumber
        )
     
    def _build_iterator(self, now):
        # builds the request and retrieves the page
        rkwargs = dict(
            stream=False,
            verify=self.verify_cert,
            timeout=self.polling_timeout
        )

        r = requests.get(
            self.url,
            **rkwargs
        )
        
        try:
            r.raise_for_status()
        except:
            LOG.debug('%s - exception in request: %s %s',
                      self.name, r.status_code, r.content)
            raise

        data = json.loads(r.content)
        for i in data['data']['prefixes']:
            yield i['prefix']
        # parse the page
        #html_soup = bs4.BeautifulSoup(r.content, "lxml")
        #table = html_soup.find(id="prefixes")
        #result = table.find_all("a")
        
        #return result
           
    def _process_item(self, item):
        # called on each item returned by _build_iterator
        # it should return a list of (indicator, value) pairs
        net = item
        if net is None:
            LOG.error('%s - no network', self.name)
            return []
        
        indicator = net
        value = {
            'type': 'IPv4',
            'confidence': 100
        }
        
        return [[indicator, value]]